GitLab to Glip Webhook Relay
============================

Glip does not yet (as of September 2015) have a working integration with
GitLab. It does, however, have a generic webhook interface.

This is a very simple library that can be loaded into the Google App Engine
to perform that proxying.

To deploy an instance for yourself, go to
[console.developers.google.com](https://console.developers.google.com) and
create a new project. Edit the first line of [app.yaml](app.yaml) and replace
`gitlabtoglip` with your new project name. Then run `goapp deploy` to deploy
it to the Google App Engine.

To configure the webhooks, go to Glip and add a new integration:
"Glip WebHooks". It will give you a url that looks like:

    https://hooks.glip.com/webhook/<long-id>

Take note of that long id. In GitLab, go to Settings, then Web Hooks.
In the URL box, put:

    https://<project-name>.appspot.com/<long-id>

Ensure that only the "Push events" box is checked, since this library does not
(yet!) support any other event types.

Add the web hook, test it, and you're good to go.
