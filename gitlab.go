package gitlabtoglip

import (
	"encoding/json"
	"fmt"
	"strings"
)

type Push struct {
	ObjectKind        string     `json:"object_kind"`
	Before            string     `json:"before"`
	After             string     `json:"after"`
	Ref               string     `json:"ref"`
	UserId            int        `json:"user_id"`
	UserName          string     `json:"user_name"`
	UserEmail         string     `json:"user_email"`
	ProjectId         int        `json:"project_id"`
	Repository        Repository `json:"repository"`
	Commits           []Commit   `json:"commits"`
	TotalCommitsCount int        `json:"total_commits_count"`
}

type Repository struct {
	Name            string `json:"name"`
	Url             string `json:"url"`
	Description     string `json:"description"`
	Homepage        string `json:"homepage"`
	GitHttpUrl      string `json:"git_http_url"`
	GitSshUrl       string `json:"git_ssh_url"`
	VisibilityLevel int    `json:"visibility_level"`
}

type Commit struct {
	Id        string `json:"id"`
	Message   string `json:"message"`
	Timestamp string `json:"timestamp"`
	Url       string `json:"url"`
	Author    Person `json:"author"`
}

type Person struct {
	Name  string `json:"name"`
	Email string `json:"email"`
}

func (p Push) ToGlip() *Glip {
	if len(p.Commits) == 0 {
		return nil
	}

	g := &Glip{Activity: "New Changesets Added"}
	if len(p.Commits) == 1 {
		g.Title = fmt.Sprintf("%d changeset was added to [%s](%s).", len(p.Commits), p.Repository.Name, p.Repository.Homepage)
	} else {
		g.Title = fmt.Sprintf("%d changesets were added to [%s](%s).", len(p.Commits), p.Repository.Name, p.Repository.Url)
	}

	for _, commit := range p.Commits {
		g.Body = g.Body + fmt.Sprintf("* [%s](%s) (%s)\n", clipMessage(commit.Message), commit.Url, commit.Author.Name)
	}
	return g
}

func NewPush(data []byte) *Push {
	var p Push
	err := json.Unmarshal(data, &p)
	if err != nil {
		return nil
	}

	if p.ObjectKind != `push` {
		return nil
	}

	return &p
}

func clipMessage(s string) string {
	firstNl := strings.IndexAny(s, "\r\n")
	if firstNl != -1 {
		return s[:firstNl]
	}
	return s
}
